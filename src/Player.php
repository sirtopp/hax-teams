<?php

declare(strict_types=1);

namespace SmartWeb\Hax;

class Player
{
    /** @var string */
    public $name;

    /** @var float */
    public $rating;

    public function __construct(string $name, float $rating)
    {
        $this->name = $name;
        $this->rating = $rating;
    }

    public function __toString(): string
    {
        return sprintf('[%s]', $this->name);
    }
}
