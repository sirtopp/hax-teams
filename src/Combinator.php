<?php

declare(strict_types=1);

namespace SmartWeb\Hax;

/**
 * Yields all team combinations for given players.
 */
class Combinator
{
    /**
     * @param Player[] $players  number indexed!
     * @return Team[][] Iterabme of pairs of teams
     */
    public function allCombinations(array $players): iterable
    {
        $players = array_column($players, null, 'name');

        $asidePlayer = null;
        if (count($players) % 2 === 0) {
            // Even number -- we remove 1 player,
            // generate all combinations of un-even teams
            // and add that player to less-numerous team. Funny, huh?
            $asidePlayer = reset($players);
            $players = array_slice($players, 1);
        }

        $team1Combinations = $this->pick(
            // Less numerous team will be team 2, because we pick CEIL(count / 2)
            (int)ceil(count($players) / 2),
            $players
        );

        foreach ($team1Combinations as $team1) {
//            $t1 = new Team(array_intersect_key($players, array_flip($team1Players)));
//            $team2 = new Team(array_diff_key($players, array_flip($team1Players)));
            $t2Players = array_diff_key(
                $players,
                array_flip(array_column($team1->getPlayers(), 'name'))
            );
            $team2 = new Team($t2Players);
            if ($asidePlayer !== null) {
                $team2 = $team2->withPlayer($asidePlayer);
            }
            yield [$team1, $team2];
        }
    }

    /**
     * @return \Traversable|Team[]
     */
    public function pick(int $n, array $players): \Traversable
    {
        if (count($players) === $n) {
            return yield new Team($players);
        }
        if (count($players) === 0 || $n === 0) {
            return yield new Team();
        }

        $firstPlayer = reset($players);
        $theRest = array_slice($players, 1);

        foreach ($this->pick($n - 1, $theRest) as $combination) {
            yield $combination->withPlayer($firstPlayer); //array_merge([$firstPlayer], $combination);
        }
        yield from $this->pick($n, $theRest);
    }
}
