<?php

namespace SmartWeb\Hax;

class Team
{
    /** @var Player[] */
    protected array $players;

    /**
     * @param Player[] $players
     */
    public function __construct(array $players = [])
    {
        $this->players = $players;
    }

    public function addPlayer(Player $player): void
    {
        $this->players[] = $player;
    }

    public function getPlayers(): array
    {
        return $this->players;
    }

    public function isSame(Team $another): bool
    {
        return $this->getNames() == $another->getNames();
    }

    /** @return string[] */
    public function getNames(): array
    {
        $names = array_column($this->players, 'name');
        sort($names);
        return $names;
    }

    public function getTotalRating(): float
    {
        return array_sum(array_column($this->players, 'rating'));
    }

    public function getAvgRating(): float
    {
        return $this->getTotalRating() / count($this->players);
    }

    public function getPlayerCount(): int
    {
        return count($this->players);
    }

    public function withPlayer(Player $player): Team
    {
        return new Team(array_merge($this->players, [$player]));
    }

    public function __toString(): string
    {
        return sprintf("(%s -- %.2f)", implode(', ', $this->getNames()), $this->getAvgRating());
    }
}
