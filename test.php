<?php

require_once "vendor/autoload.php";

use SmartWeb\Hax\Combinator;
use SmartWeb\Hax\Player;
use SmartWeb\Hax\Team;

$players = array_column([
    new Player('MaddoHatto', 2030.6284539526),
    new Player('K-rol', 1378.9800053109),
    new Player('hubigz', 1375.0646110164),
    new Player('Nelson Mandela', 1273.3640801741),
    new Player('Toperz', 1224.0487753153),
    new Player('ToPP', 1034.4765300904),
    new Player('Amman', 810.64276880767),
    new Player('Panda', 767.44843330595),
    new Player('rybak', 752.59576222166),
    new Player('adamaru', 749.1027909822),
], null, 'name');

/** @param Team[] $teams */
function normalizeLineup($teams) {
    $teams = array_map('normalizeTeam', $teams);

    return ($teams[0]->getAvgRating() < $teams[1]->getAvgRating())
        ? [$teams[0], $teams[1]]
        : [$teams[1], $teams[0]];
}

function normalizeTeam(Team $t): Team {
    $playersByName = array_column($t->getPlayers(), null, 'name');
    ksort($playersByName);
    return new Team(array_values($playersByName));
}

$expected = [
    // assume lower ranked team first
    new Team([
        $players['MaddoHatto'],
        $players['adamaru'],
        $players['Panda'],
        $players['K-rol'],
        $players['rybak'],
    ]),
    new Team([
        $players['Amman'],
        $players['Toperz'],
        $players['ToPP'],
        $players['Nelson Mandela'],
        $players['hubigz'],
    ]),
];


////////////////////// START CODING HERE
$time = microtime(true);

$combinator = new Combinator();
$playersTest = array_values(array_slice($players, 0, 3));
$playersTest = [
    new Player('A', 1),
    new Player('B', 1),
    new Player('C', 1),
    new Player('D', 1),
    new Player('E', 1),
    new Player('F', 1),
    new Player('G', 1),
    new Player('H', 1),
    new Player('I', 1),
    new Player('J', 1),
];
$count = 0;
$bestDelta = PHP_INT_MAX;
$result = [];
foreach ($combinator->allCombinations($players) as list($t1, $t2)) {
    $delta = abs($t1->getAvgRating() - $t2->getAvgRating());
    if ($delta < $bestDelta) {
        $result = [$t1, $t2];
        $bestDelta = $delta;
    }
    printf("%d\t$t1 /// $t2%s\n", ++$count, ($bestDelta === $delta) ? ' <---' : '');
}

$time = microtime(true) - $time;
///////////////////// END CODING HERE



$expected = normalizeLineup($expected);
$result = normalizeLineup($result);

printf("Expecting:\n\t%s\n\t%s\n", $expected[0], $expected[1]);
printf("Calculated (%.2fs):\n\t%s\n\t%s\n", $time, $result[0], $result[1]);

$correct = ($result[0]->isSame($expected[0]) && $result[1]->isSame($expected[1]));

printf("\nResult: %s\n\n", $correct ? "Correct!" : "xx WRONG xx");
